package com.nlp.spelling;

import java.util.ArrayList;
import java.util.Arrays;

public class App 
{
	private final String vowels[] = new String[] {"a", "e", "ı", "i", "o", "ö", "u", "ü"};
	
    public static void main( String[] args )
    {
    	if (args.length < 1) {
    		System.out.println("Eksik parametre");
    		return;
    	}
    	
    	App app = new App();
    	ArrayList<String> syllables = new ArrayList<String>();
        
    	String word = String.join(" ", args);
        
        word = word.replaceAll("[^A-Za-z0-9şığçüö\\s]", "");
        
        for (String s: word.split(" ")) {
        	if (s.trim().length() > 0) {
        		syllables.add(app.spelling(s));
        	}
        }
        
        System.out.println(syllables);
    }
    
    public String spelling(String word) {
    	int i;
    	
    	for(i = 0; i < word.length(); i++) {
    		
    		if (Arrays.asList(vowels).contains(String.valueOf(word.charAt(i)))) {
    			
    			try {
    				if (Arrays.asList(vowels).contains(String.valueOf(word.charAt(i+1)))) {
    					return word.substring(0, i+1) + "-" + spelling(word.substring(i+1, word.length()));
    				}
    				else if (Arrays.asList(vowels).contains(String.valueOf(word.charAt(i+2)))) {
    					return word.substring(0, i+1) + "-" + spelling(word.substring(i+1, word.length()));
    				}
    				else if (Arrays.asList(vowels).contains(String.valueOf(word.charAt(i+3)))) {
    					return word.substring(0, i+2) + "-" + spelling(word.substring(i+2, word.length()));
    				}
    				else if (word.substring(i+1, i+4) == "str" || word.substring(i+1, i+4) == "ktr" || word.substring(i+1, i+4) == "ntr") {
    					return word.substring(0, i+2) + "-" + spelling(word.substring(i+2, word.length()));
    				}
    				else {
    					return word.substring(0, i+3) + "-" + spelling(word.substring(i+3, word.length()));
    				}
    				
    			} catch (StringIndexOutOfBoundsException e) {
    				return word;
    			}
    		}
    	}
    	
    	return word;
    }
}
